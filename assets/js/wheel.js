let canvas = document.getElementById("canvas");

let sections = [];

let colors = [];

let apiUrl = "https://feguard.safaridragon.xyz/api/wheel/"
let wheels = null;
let frame = null;
let token = "";
let lang = "en";
let translation = [
    {
        "en": {
            "t&c": "Terms and Condition",
            "noSpin": "You've not available spins left, please deposit and enjoy our lucky spin!",
            "usedUp": "You've used up all your available spin! Deposit today and stand a chance to win our grand prize.",
            "pleaseLogin": "Please login to enjoy our lucky spin",
            "miss": "Sorry, you've to try harder next time, grand prize awaiting you!",
            "congrats": "Congratulations, you've won ",
            "remain": "Remaining Spin: ",
            "requirement": "Requirement:",
            "req_examples": ['Deposit a total of RM150 to get 1 spin (Refer example 3)', 'Spin requires turnover of main bonus to be achieved in order to withdraw together. (Refer example 1 & 2)'],
            "tnc": "T&C",
            "nameSpin": "Lucky Spin Wheel",
            "tnc_examples": "All spins are resetted on a monthly basis",
            "exp": "Examples",
            "exp_examples": ['Player deposit RM150 and took turnover x1 bonus. Player can spin 1 time and receive credit straight' +
            '                away. Once turnover x1 bonus is achieved, can withdraw full amount including bonus wheel',
                'Player deposit RM1500 and took turnover x5 bonus. Player can spin 10 times and receive credit straight' +
                '                away. Once turnover x5 bonus is achieved, can withdraw full amount including bonus wheel', 'Player deposits RM30, 5 times. On his 5th RM30 deposit, he will be able to claim 1 spin.']
        },
        "ms": {
            "t&c": "Terma dan Syarat",
            "noSpin": "Anda tidak mempunyai baki lucky spin, sila buat deposit dan nikmatilah hadiah anda",
            "usedUp": "Anda telah menghabiskan baki lucky spin, sila buat deposit dan nikmatilah hadiah ganjaran!",
            "pleaseLogin": "Sila log masuk untuk nikmati lucky spin kita ! Hadiah yang mewah menanti anda",
            "miss": "Minta maaf, sila cuba lain kali lagi",
            "congrats": "Tahniah, anda telah menang ",
            "remain": "Baki Spin: ",
            "requirement": "Keperluan:",
            "req_examples": ['Deposit RM150 untuk dapat 1 spin (rujuk contoh 3)', 'Spin memerlukan turnover main bonus dicapai agar dapat buat pengeluaran bersama (rujuk contoh 1&2)'],
            "tnc": "T&C",
            "nameSpin": "Roda Lucky",
            "tnc_examples": "Semua putaran diset semula setiap bulan",
            "exp": "Contoh",
            "exp_examples": ['Member yang buat deposit RM150 akan dapat 1 spin dan dapat kredit selepas spin roda. Apabila turnover x1 dicapai, member boleh buat pengeluaran sepenuhnya dengan segera termasuk kredit roda',
                'Member yang buat deposit RM1500 dan dapat bonus yang memerlukan turnover x5 akan dapat buat spin 10 kali dan dapat kredit serta merta. Apabila turnover x5 dicapai, member boleh buat pengeluaran sepenuhnya termasuk amaun roda. Member yang buat deposit RM 30 x 5 kali akan dapat 1 kali spin']
        },
        "zh": {
            "t&c": "条规",
            "noSpin": "您没有任何剩余的旋转次数，请进行存款以便享有旋转机会！",
            "usedUp": "您已用尽了幸运旋转次数，请进行存款以便享有旋转机会！",
            "pleaseLogin": "请登入，以进行旋转！",
            "miss": "抱歉，您没获得任何的奖励！",
            "congrats": "恭喜您，获取了 ",
            "remain": "剩余次数: ",
            "requirement": "游戏条件:",
            "req_examples": ['存款RM150获得一次机会（例: 3）', '任何旋转次数都需要 1x 流水以便可以全额提款（例：1&2）'],
            "tnc": "游戏规则:",
            "nameSpin": "幸运轮盘",
            "tnc_examples": "轮盘的奖品会在每月进行重置",
            "exp": "例",
            "exp_examples": ['存入RM150的会员将获得1次旋转并在旋转后获得积分。当流水达到x1时，会员可以立即全额提现，包括轮盘的信用额度',
                '存款RM1500并获得需要的流水x5的红利会员，将能够旋转10次并获得即时奖品。达到流水x5时，会员可以全额提现，包括轮盘的款项。存款RM 30 x 5次的会员将获得1次旋转']
        }
    }
]


function repaint(angle) {
    let r = Math.min(innerWidth, innerHeight) / 3.00 | 0;
    if (wheels === null) {
        wheels = [];
        for (let selected = 0; selected < sections.length; selected++) {
            let c = document.createElement("canvas");
            c.width = c.height = 2 * r + 10;
            let ctx = c.getContext("2d"), cx = 5 + r, cy = 5 + r;
            let g = ctx.createRadialGradient(cx, cy, 0, cx, cy, r);
            g.addColorStop(0, "rgba(0,0,0,0)");
            g.addColorStop(1, "rgba(0,0,0,0.5)");
            for (let i = 0; i < sections.length; i++) {
                let a0 = 2 * Math.PI * i / sections.length;
                let a1 = a0 + 2 * Math.PI / (i == 0 ? 1 : sections.length);
                let a = 2 * Math.PI * (i + 0.5) / sections.length;
                ctx.beginPath();
                ctx.moveTo(cx, cy);
                ctx.arc(cx, cy, r, a0, a1, false);
                ctx.fillStyle = colors[i % 4];
                ctx.fill();
                ctx.fillStyle = g;
                ctx.fill();
                ctx.save();
                if (i == selected) {
                    ctx.fillStyle = "#E9C518";
                } else {
                    if (sections[i] === "Mystery Gift") {
                        let gradient = ctx.createLinearGradient(0, 0, c.width, 0);
                        gradient.addColorStop("0", " yellow");
                        gradient.addColorStop("1.0", "red");
                        ctx.fillStyle = gradient;
                    } else {
                        ctx.fillStyle = "#fff";
                    }
                }
                ctx.font = "bold " + r / sections.length * 1.3 + "px kanit";
                ctx.textAlign = "center";
                ctx.textBaseline = "middle";
                ctx.translate(cx, cy);
                ctx.rotate(a);
                ctx.fillText(sections[i], r * 0.62, 0);
                ctx.restore();
            }
            wheels.push(c);
        }
    }
    if (frame === null) {
        frame = document.createElement("canvas");
        frame.width = frame.height = 10 + 2 * r * 1.25 | 0;
        let ctx = frame.getContext("2d"), cx = frame.width / 2, cy = frame.height / 2;
        ctx.shadowOffsetX = r / 80;
        ctx.shadowOffsetY = r / 80;
        ctx.shadowBlur = r / 40;
        ctx.shadowColor = "rgba(255,255,255,0.5)";
        ctx.beginPath();
        ctx.arc(cx, cy, r * 1.025, 0, 2 * Math.PI, true);
        ctx.arc(cx, cy, r * 0.975, 0, 2 * Math.PI, false);
        ctx.fillStyle = "#e6e920";
        ctx.fill();
        ctx.shadowOffsetX = r / 40;
        ctx.shadowOffsetY = r / 40;
        g = ctx.createRadialGradient(cx - r / 7, cy - r / 7, 0, cx, cy, r / 3);
        g.addColorStop(0, "#4549d3");
        g.addColorStop(0.2, "#383DA9");
        g.addColorStop(1, "#1E235C");
        ctx.fillStyle = g;
        ctx.beginPath();
        ctx.arc(cx, cy, r / 3.5, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.translate(cx, cy);
        ctx.rotate(Math.PI - 0.2);
        ctx.beginPath();
        ctx.moveTo(-r * 1.1, -r * 0.05);
        ctx.lineTo(-r * 0.9, 0);
        ctx.lineTo(-r * 1.1, r * 0.05);
        ctx.fillStyle = "#F44";
        ctx.fill();
    }
    canvas.width = innerWidth;
    canvas.height = innerHeight;
    let cx = innerWidth / 2, cy = innerHeight / 2;
    let ctx = canvas.getContext("2d");
    let selected = (Math.floor((-0.2 - angle) * sections.length / (2 * Math.PI))
        % sections.length);
    if (selected < 0) selected += sections.length;
    ctx.save();
    ctx.translate(cx, cy);
    ctx.rotate(angle);
    ctx.translate(-wheels[selected].width / 2, -wheels[selected].height / 2);
    ctx.drawImage(wheels[selected], 0, 0);
    ctx.restore();
    ctx.drawImage(frame, cx - frame.width / 2, cy - frame.height / 2);
}

let angle = 0, running = false;

function spinTo(winner, duration) {
    let final_angle = (-0.2) - (0.5 + winner) * 2 * Math.PI / sections.length;
    let start_angle = angle - Math.floor(angle / (2 * Math.PI)) * 2 * Math.PI - 5 * 2 * Math.PI;
    let start = performance.now();

    function frame() {
        let now = performance.now();
        let t = Math.min(1, (now - start) / duration);
        t = 3 * t * t - 2 * t * t * t; // ease in out
        angle = start_angle + t * (final_angle - start_angle);
        repaint(angle);
        if (t < 1) {
            requestAnimationFrame(frame);
        } else {
            $('#winPrize').modal({
                fadeDuration: 100
            });
            running = false;
            let spinLeft = parseInt($('#spinLeft').html());
            if (!isNaN(spinLeft)) {
                spinLeft = spinLeft - 1;
                $('#spinLeft').html(spinLeft);
            }
        }
    }

    requestAnimationFrame(frame);
    running = true;

}

async function init() {
    try {
        let token = getParameterByName("token");
        let language = getParameterByName("lang");
        lang = language == null ? "en" : language;
        $('#tncBtn').text(translation[0][lang]["t&c"])
        $('#req_title').html(translation[0][lang]["requirement"])
        $('#tnc_title').html(translation[0][lang]["tnc"])
        $('#exp_title').html(translation[0][lang]["exp"])
        $('#remain').html(translation[0][lang]["remain"])
        $('#nameSpin').html(translation[0][lang]["nameSpin"])
        var req = [];
        var exp = [];
        $.each(translation[0][lang]["req_examples"], function (i, k) {
            req.push("<li>");
            req.push(k);
            req.push("</li>");
        });
        $('#req_examples').append(req.join(''))
        $('#tnc_examples').append("<li>" + translation[0][lang]["tnc_examples"] + "</li>");
        $.each(translation[0][lang]["exp_examples"], function (i, k) {
            exp.push("<li>");
            exp.push(k);
            exp.push("</li>");
        });
        $('#exp_examples').append(exp.join(''))
        sections = [];
        const draws = await numOfDraws(token);
        const res = await wheelLeaves()
        if (res.success) {
            $.each(res.wheels, function (i, k) {
                sections.push(k.name)
                colors.push(k.color_code)
            });

            repaint(angle);
            let csz = null;
            setInterval(function () {
                let sz = innerWidth + "/" + innerHeight;
                if (csz !== sz) {
                    csz = sz;
                    wheels = frame = null;
                    repaint(angle);
                }
            }, 10);
        } else {
            $('#error_msg').html(translation[0][lang].pleaseLogin)
            $('#errorModal').modal({
                fadeDuration: 100
            });
        }
        if (draws.success) {
            if (draws.available_spins > 0) {
                $('#spinLeft').html(draws.available_spins);
            }
            var acc_fund = draws.accumulated_funds
            var fund_needed = draws.funds_per_spin
            var pointhtml = '';
            if (lang == "en") {
                pointhtml =  "(Points Required to Spin : " + fund_needed + ")  Your Points : " + acc_fund;
            } else if (lang == "ms") {
                pointhtml = "(Points Perlu untuk Spin : " + fund_needed + ")  Points anda: " + acc_fund;
            } else {
                pointhtml ="(积分需求 : " + fund_needed + ") 您的积分: " + acc_fund;
            }

            $('#pointNeed').html(pointhtml);

        } else {
            $('#error_msg').html(translation[0][lang].noSpin)
            $('#errorModal').modal({
                fadeDuration: 100
            });
        }
    } catch (err) {
        console.log(err);
    }
}

function spinNow(token) {
    return $.ajax({
        url: apiUrl + "spin",
        method: "POST",
        headers: {
            "Authorization":
                "Bearer " + token,
        },
    });
}

function numOfDraws(token) {
    return $.ajax({
        url: apiUrl + "draws",
        method: "GET",
        headers: {
            "Authorization":
                "Bearer " + token,
        },
    });
}

function wheelLeaves(token) {
    return $.ajax({
        url: apiUrl + "leaves",
        method: "GET",
        headers: {
            "Authorization":
                "Bearer " + "token",
        },
    });
}


init();

function randomInt(min, max) {
    return min + Math.floor((max - min) * Math.random());
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


$('#tncBtn').on("click", function () {
    $('#termandcondition').modal({
        fadeDuration: 100
    });
});

canvas.onmousedown = async function () {
    let arrIndexMatch = [];
    if (!running) {
        let token = getParameterByName("token");
        let spinRes = await spinNow(token);
        if (spinRes.success) {
            if (spinRes.spin_result != null) {
                $.each(sections, function (i, k) {
                    if (k === spinRes.spin_result.name) {
                        arrIndexMatch.push(i);
                    }
                });
                console.log(arrIndexMatch);
                if (spinRes.spin_result.name != "MISS") {
                    $('#prizeWin').html(translation[0][lang].congrats + spinRes.spin_result.name + " !");
                } else {
                    $('#prizeWin').html(translation[0][lang].miss);
                }

                let spinSection = arrIndexMatch.length > 1 ? arrIndexMatch[randomInt(0, arrIndexMatch.length)] : arrIndexMatch[0];
                console.log(spinSection)
                spinTo(spinSection, 5000);
            }
        } else {
            $('#error_msg').html(translation[0][lang].usedUp)
            $('#errorModal').modal({
                fadeDuration: 100
            });
        }
    }
};

